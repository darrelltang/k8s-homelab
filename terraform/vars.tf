variable ssh_user {
    type = string
    default = ""
}
variable ssh_password {
    type = string
    default = ""
}