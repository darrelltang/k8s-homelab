provider "proxmox" {
    pm_api_url          = "https://darrellandranae.asuscomm.com:8006/api2/json"
    pm_user             = "terraform@pve"
    pm_tls_insecure     = true
}

terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "TangFamily"

    workspaces {
      name = "k8s-homelab"
    }
  }
}

resource "proxmox_vm_qemu" "k8s-cluster" {
    count           = 2
    vmid            = 200+count.index
    name            = "microk8s-${count.index+1}"
    desc            = "microk8s running on Ubuntu 20.04"
    target_node     = "proxmox"
    clone           = "ubuntu-20.04-microk8s"
    full_clone      = true
    agent           = 1
    memory          = 1024

    os_type         = "ubuntu"

    connection {
        type        = "ssh"
        host        = self.ssh_host
        user        = var.ssh_user
        password    = var.ssh_password
    }

    provisioner "remote-exec" {
        inline = [
          "sudo hostnamectl set-hostname microk8s-${count.index+1}",
          "sudo ufw allow in on cni0 && sudo ufw allow out on cni0",
          "sudo ufw default allow routed",
          "microk8s enable dashboard dns ingress",
          "sudo microk8s kubectl proxy --accept-hosts=.* --address=0.0.0.0 & "
        ]
  }
}