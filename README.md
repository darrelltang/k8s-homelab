# k8s-homelab

Create a `vars.json` file to contain your passwords:

```
{
    "variables" : {
        "proxmox_username": "",
        "proxmox_password": "",
        "gitlab_deploy_user": "",
        "gitlab_deploy_token": ""
    }
}
```